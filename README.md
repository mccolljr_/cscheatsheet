# Data Structures

#### Array

**Space**

`O(n)`

**Average**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`Θ(1)`|`Θ(n)`|`Θ(n)`|`Θ(n)`|

**Worst**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`O(1)`|`O(n)`|`O(n)`|`O(n)`|
---

#### Stack

**Space**

`O(n)`

**Average**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`Θ(n)`|`Θ(n)`|`Θ(1)`|`Θ(1)`|

**Worst**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`O(n)`|`O(n)`|`O(1)`|`O(1)`|
---

#### Queue

**Space**

`O(n)`

**Average**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`Θ(n)`|`Θ(n)`|`Θ(1)`|`Θ(1)`|

**Worst**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`O(n)`|`O(n)`|`O(1)`|`O(1)`|
---

#### Singly-/Doubly-Linked Lists

**Space**

`O(n)`

**Average**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`Θ(n)`|`Θ(n)`|`Θ(1)`|`Θ(1)`|

**Worst**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`O(n)`|`O(n)`|`O(1)`|`O(1)`|
---

#### Skip List

**Space**

`O(n log(n))`

**Average**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`Θ(log(n))`|`Θ(log(n))`|`Θ(log(n))`|`Θ(log(n))`|

**Worst**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`O(n)`|`O(n)`|`O(n)`|`O(n)`|
---

#### Hash Table

Hash Tables are a powerful data structure offering fast insertion and lookup, but
there are caveats. First, a Hash Table ust use a good hash function. This means that
the hash function must create as close to an even distribution among the indices
of the Hash Table as possible. The second thing that can make-or-break a Hash Table
implementation is the handling of collisions when they do happen.

There are several approaches to this, the easiest to implement being **Linear Probing**
and **Chaining**.

A Hash Table's worst case times are seen when every element hashes to the same value,
thereby reducing a Hash Table to what is effectively an Array or Linked List.

**Space**

`O(n)`

**Average**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`---`|`Θ(1)`|`Θ(1)`|`Θ(1)`|

**Worst**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`---`|`O(n)`|`O(n)`|`O(n)`|
---

#### Cartesian Tree

**Space**

`O(n)`

**Average**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`---`|`Θ(log(n))`|`Θ(log(n))`|`Θ(log(n))`|

**Worst**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`---`|`O(n)`|`O(n)`|`O(n)`|
---

#### Binary Search Tree

*not balanced*

**Space**

`O(n)`

**Average**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`Θ(log(n))`|`Θ(log(n))`|`Θ(log(n))`|`Θ(log(n))`|

**Worst**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`O(n)`|`O(n)`|`O(n)`|`O(n)`|
---

#### B-Tree, Red-Black Tree, AVL Tree

*self-balancing binary search trees*

There are many forms of self-balancing Binary Search Trees, but they each share
common time complexities for both best- and worst-case on all operations.

**Space**

`O(n)`

**Average**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`Θ(log(n))`|`Θ(log(n))`|`Θ(log(n))`|`Θ(log(n))`|

**Worst**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`O(log(n))`|`O(log(n))`|`O(log(n))`|`O(log(n))`|
---

#### Splay Tree

**Space**

`O(n)`

**Average**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`---`|`Θ(log(n))`|`Θ(log(n))`|`Θ(log(n))`|

**Worst**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`---`|`O(log(n))`|`O(log(n))`|`O(log(n))`|
---

#### KD Tree

**Space**

`O(n)`

**Average**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`Θ(log(n))`|`Θ(log(n))`|`Θ(log(n))`|`Θ(log(n))`|

**Worst**

|Access|Lookup|Insert|Delete|
|---|---|---|---|
|`O(n)`|`O(n)`|`O(n)`|`O(n)`|
---

# Sorting Algorithms
---
|Algorithm|Best Time|Avg. Time|Worst Time|Space|
|---|---|---|---|---|
|Quicksort|`Ω(n log(n))`|`Θ(n log(n))`|`O(n^2)`|`O(log(n))`|
|Mergesort|`Ω(n log(n))`|`Θ(n log(n))`|`O(n log(n))`|`O(n)`|
|Heapsort|`Ω(n log(n))`|`Θ(n log(n))`|`O(n log(n))`|`O(1)`|
|Bubble Sort|`Ω(n)`|`Θ(n^2)`|`O(n^2)`|`O(1)`|
|Insertion Sort|`Ω(n)`|`Θ(n^2)`|`O(n^2)`|`O(1)`|
---

# Resources
*Big-O Cheat Sheet* -- [bigocheatsheet.com](http://bigocheatsheet.com/)